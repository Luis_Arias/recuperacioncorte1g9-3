package com.example.recuperacincorte1g93_java;

import java.util.Random;

public class Cotizacion {

    private int folio;
    private String descripcion;
    private float valorAuto;
    private float porEnganche;
    private int plazo;

    public Cotizacion(int folio, String descripcion, float valorAuto, float porEnganche, int plazo)
    {
        this.folio = folio;
        this.descripcion = descripcion;
        this.porEnganche = porEnganche;
        this.plazo = plazo;
        this.valorAuto = valorAuto;
    }

    public int getFolio(){
        return folio;
    }

    public void setFolio(){
        this.folio = folio;
    }

    public String getDescripcion()
    {
        return descripcion;
    }

    public void setDescripcion(){
        this.descripcion = descripcion;
    }
    public float getValorAuto(){
        return valorAuto;
    }

    public void setValorAuto(){
        this.valorAuto = valorAuto;
    }
    public float getPorEnganche(){
        return porEnganche;
    }
    public void setPorEnganche(){
        this.porEnganche = porEnganche;
    }

    public int getPlazo(){
        return plazo;
    }

    public void setPlazo(){
        this.plazo = plazo;
    }

    public int generarFolio(){

        Random val = new Random();
        int Numero = val.nextInt(900);
        folio = Numero;
        return folio;
    }

    public float  calcularEnganche(){
        float Porcentaje= 0.0f;

        porEnganche = valorAuto * Porcentaje;

        return porEnganche;
    }

    public float calcularPagoMensual(){
        float Total= 0.0f;

        if(plazo == 12){
           Total=valorAuto/12;
        }
        else if(plazo == 18){
            Total = valorAuto/18;
        }
        else if (plazo ==24){
            Total = valorAuto/24;
        }
        else if (plazo==36){
            Total = valorAuto/36;
        }

        return Total;
    }


}
